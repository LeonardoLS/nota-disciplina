/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.java.nota.disciplina;

/**
 *
 * @author Aluno
 */
public class aluno {
    private String nome;
    private double[] notas = new double[3];
    private double[] historico = new double[3];
    private double media;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double[] getNotas(int i) {
        return notas;
    }

    public void setNotas(double notas, int i) {
        this.notas[i] = notas;
    }

    public double getHistorico(int i) {
        return historico[i];
    }

    public void setHistorico(double historico, int i) {
        this.historico[i] = historico;
    }

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }
}
