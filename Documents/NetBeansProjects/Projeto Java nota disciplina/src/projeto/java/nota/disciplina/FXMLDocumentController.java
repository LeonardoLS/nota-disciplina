/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.java.nota.disciplina;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TextField cadastroNome;
    @FXML
    private TextField cadastroHist1;
    @FXML
    private TextField cadastroHist2;
    @FXML
    private TextField cadastroHist3;
    @FXML
    private TextField cadastroNota1;
    @FXML
    private TextField cadastroNota2;
    @FXML
    private TextField cadastroNota3;
    @FXML
    private TextField calcularNota1;
    @FXML
    private TextField calcularNota2;
    @FXML
    private TextField calcularNota3;
    @FXML
    private TextField numAluno;
    @FXML
    private Button cadastrar;
    @FXML
    private Button calcular;
    @FXML
    private Button mostHistorico;
    @FXML
    private Button mostTurma;
    @FXML
    private Label calcularMedia;
    Prog2 c = new Prog2();
    
    
    @FXML
    private void cadastrar() {
       c.ler(cadastroNome.getText(),Double.parseDouble(cadastroHist1.getText()),Double.parseDouble(cadastroHist2.getText()),Double.parseDouble(cadastroHist3.getText()),Double.parseDouble(cadastroNota1.getText()),Double.parseDouble(cadastroNota2.getText()),Double.parseDouble(cadastroNota3.getText()));
       cadastroNome.setText("");
       cadastroHist1.setText("");
       cadastroHist2.setText("");
       cadastroHist3.setText("");
       cadastroNota1.setText("");
       cadastroNota2.setText("");
       cadastroNota3.setText("");
    }
    @FXML
    private void calcular() {
       double a;
       a = c.calcular(Double.parseDouble(calcularNota1.getText()),Double.parseDouble(calcularNota2.getText()),Double.parseDouble(calcularNota3.getText()));
       calcularMedia.setText(""+a);
       calcularNota1.setText("");
       calcularNota2.setText("");
       calcularNota3.setText("");
    }
    @FXML
    private void historico() {
       c.historico(Integer.parseInt(numAluno.getText()));
       numAluno.setText("");
    }
    @FXML
    private void printar() {
       c.printar();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
