/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.java.nota.disciplina;

/**
 *
 * @author Aluno
 */
import java.util.Arrays;
import java.util.Scanner;
public class Prog2 {
     Scanner ent = new Scanner(System.in);
     int i=0,x,sair,teste;
     aluno[] turma;
    public Prog2() {
        this.teste = 0;
        this.sair = 2;
        this.turma = new aluno[50];
        for(x=0;x<50;x++){
             turma[x] = new aluno();
        }
    }
         void ler(String nome, double hist1, double hist2, double hist3, double nota1, double nota2, double nota3){
             turma[i].setNome(nome);
             turma[i].setHistorico(hist1,0);
             turma[i].setHistorico(hist2,1);
             turma[i].setHistorico(hist3,2);
             turma[i].setNotas(nota1,0);
             turma[i].setNotas(nota2,1);
             turma[i].setNotas(nota3,2);
             turma[i].setMedia(((nota1*2)+(nota2*4)+(nota3*4))/10);
             i++;
     }
        double calcular(double nota1,double nota2,double nota3){
            return (((nota1*2)+(nota2*4)+(nota3*4))/10);
         }
        void historico(int teste){
             for(x=0;x<i;x++){
                 System.out.println(x + ". "+turma[x].getNome());
             }
             System.out.println("1º trimestre:"+turma[teste].getHistorico(0)+"\n2º trimestre:"+turma[teste].getHistorico(1)+"\n3º trimestre:"+turma[teste].getHistorico(2));
        }
        
        void printar(){
           for(x=0;x<i;x++){
                 System.out.println(x + ". "+turma[x].getNome());
                 System.out.println("Histórico:");
                 System.out.println("Nota 1:" +turma[x].getHistorico(0));
                 System.out.println("Nota 2:" +turma[x].getHistorico(1));
                 System.out.println("Nota 3:" +turma[x].getHistorico(2));
                 System.out.println("Notas atuais:");
                 System.out.println("Nota 1:" +Arrays.toString(turma[x].getNotas(0)));
                 System.out.println("Nota 2:" +Arrays.toString(turma[x].getNotas(1)));
                 System.out.println("Nota 3:" +Arrays.toString(turma[x].getNotas(2)));
                 System.out.println("Media:");
                 System.out.println(turma[x].getMedia());
             }
        }
}
